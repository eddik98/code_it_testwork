const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');
const api = require('./routes');

const app = express();

// logger
app.use(morgan('dev'));

app.use(cors({ credentials: true, origin: true }));

app.use(express.static(path.join(__dirname, '/frontend')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.render('index.html');
});

app.use('/api', api());

const server = app.listen(3000, () => {
  console.log('App running on port.', server.address().port);
});


module.exports = server;
