const db = require('../db')();
const { generateRandomId } = require('../utils');

/**
 * GET info about user
 */
exports.get = (req, res) => {
  const { id } = req.params;

  if (!id) {
    res.status(400).send({ message: 'Id is undefined' });
  }

  const result = db.get('user')
    .find({ id })
    .value();

  return res.status(200).send({ user: { ...result } });
};

/**
 * Create user
 */
exports.create = (req, res) => {
  const { name } = req.body;

  if (!name) {
    res.status(400).send({ message: 'Name is undefined' });
  }

  const id = generateRandomId();
  db.get('user')
    .push({ id, name })
    .write();

  return res.status(201).send({ user: { id, name } });
};

/**
 * Update user
 */
exports.update = (req, res) => {
  const { data } = req.body;
  const { id } = req.params;

  if (!id || !data) {
    res.status(400).send({ message: 'Id or data is undefined' });
  }

  const result = db('user')
    .find({ id })
    .assign({ ...data })
    .value();

  return res.status(200).send({ user: { ...result } });
};

/**
 * Delete user
 */
exports.delete = (req, res) => {
  const { id } = req.params;

  if (!id) {
    res.status(400).send({ message: 'Id is undefined' });
  }

  db.get('user')
    .find({ id })
    .remove()
    .write();

  return res.status(200);
};
