const { Router } = require('express');
const UserController = require('../controllers/user');

module.exports = () => {
  const api = Router();

  api.get('/user/:id', UserController.get);
  api.post('/user', UserController.create);
  api.put('/user/:id', UserController.get);
  api.delete('/user/:id', UserController.create);

  // api.get('/user/:id/game/:gameId', Game.get);
  // api.post('/user/:id/game', Game.create);

  api.get('/', (req, res) => {
    res.json('Hello world!');
  });

  return api;
};
