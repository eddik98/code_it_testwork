const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const path = require('path');

module.exports = () => {
  const adapter = new FileSync(path.join(`${__dirname}/db.json`));
  const db = low(adapter);

  db.defaults({ user: [] })
    .write();

  return db;
};
