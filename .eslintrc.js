module.exports = {
  "extends": ["airbnb-base"],
  "env": {
    "es6": true
  },
  "rules": {
    "no-param-reassign": ["error", { "props": false }],
    "camelcase": "off",
    "nonblock-statement-body-position": ["error", "below"],
  }
};
