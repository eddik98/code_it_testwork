/* Presets */
const colors = [
  'red',
  'blue',
  'white',
  'green',
  'yellow',
  'grey',
  'purple',
];

const BACKEND_URL = 'http://localhost:3000/api';

const sizeAndSteps = [
  { size: 10, steps: 32 },
  { size: 6, steps: 18 },
  { size: 4, steps: 12 },
];

/**
 * Получить соседние ячейки
 *
 * @param {number} x
 * @param {number} y
 * @param {HTMLCollection} cells
 * @param {number} size
 * @return {array}
 */
function getNeighboringCells(x, y, cells, size) {
  const tmp = [];

  const neighboringCellsPositions = {
    left: { x, y: y - 1 },
    right: { x, y: y + 1 },
    top: { x: x - 1, y },
    bottom: { x: x + 1, y },
  };

  for (const i in neighboringCellsPositions) {
    const c = neighboringCellsPositions[i];

    if ((c.x >= 0 || c.x > size) && (c.y >= 0 || c.y > size)) {
      for (const j of cells) {
        const x = j.getAttribute('x');
        const y = j.getAttribute('y');

        if ((x == c.x && y == c.y)) {
          tmp.push({ coordinates: c, item: j });
        }
      }
    }
  }

  return tmp;
}

/**
 * Валидация соседних ячеек
 *
 * @param {array} neighboringCells
 * @param {string} color
 * @return {array}
 */
function validate(neighboringCells, color) {
  const tmpCells = [];

  for (const cell of neighboringCells) {
    const colorCell = cell.item.getAttribute('data-color');
    const { classList } = cell.item;

    if (color === colorCell && classList.contains('origin') === false) {
      classList.add('origin');
      tmpCells.push(cell);
    }
  }

  return tmpCells;
}

/**
 * Действие при клике
 *
 * @param {HTMLCollection} baseCells
 * @param {array} originCells
 * @param {object} target
 * @param {number} size
 * @return {array}
 */
function action(baseCells, originCells, target, size) {
  if (target.classList.contains('origin') === true) {
    return originCells;
  }

  const color = target.getAttribute('data-color');
  let tmpCells = [];

  /**
  * Рекурсивный поиск и валидация ячеек
  *
  * @param {array} orCells - originCells
  * @return {array}
  */
  const recursiveSearchAndValidation = (orCells) => {
    for (const cell of orCells) {
      const adjacentCells = getNeighboringCells(
        Number(cell.coordinates.x),
        Number(cell.coordinates.y),
        baseCells,
        size,
      );

      const resultCells = validate(adjacentCells, color);

      if (resultCells.length > 0) {
        tmpCells = tmpCells.concat(resultCells);
        recursiveSearchAndValidation(resultCells);
      } else {
        tmpCells = tmpCells.concat(resultCells);
      }
    }
  };

  recursiveSearchAndValidation(originCells);

  if (tmpCells.length === 0) {
    return originCells;
  }

  tmpCells = tmpCells.concat(originCells);

  for (const cell of tmpCells) {
    cell.item.setAttribute('data-color', color);
  }

  return tmpCells;
}

/**
 * Генерация ячейки
 *
 * @private
 * @param {string} color
 * @param {number} x
 * @param {number} y
 * @return {HTMLElement}
 */
function square(color, x, y) {
  const result = document.createElement('div');
  result.classList.add('square');
  result.setAttribute('data-color', color);

  result.setAttribute('x', x);
  result.setAttribute('y', y);

  return result;
}

/**
 * Генерация ячеек
 *
 * @param {number} size
 * @return {array}
 */
function generateSquares(size = 3) {
  const result = [];
  let _x = 0;
  let _y = 0;

  for (let l = 0; l < size * size; ++l) {
    const _color = colors
      .find(c => Math.random() > (1 - 1 / colors.length)) || colors[l % colors.length];

    const _square = square(_color, _x, _y);
    result.push(_square);

    if (l === 0) {
      _square.classList.add('origin');
    }

    _y++;

    if (_y >= size) {
      _y = 0;
      _x++;
    }
  }

  return result;
}

/**
 * Сброс цвета ячеек
 *
 * @param {object} arena
 * @param {number} size
 * @return {void}
 */
function reset(arena, size = 3) {
  arena.innerHTML = '';
  arena.setAttribute('data-size', size);

  const squares = generateSquares(size);

  for (const square of squares) {
    arena.appendChild(square);
  }
}

/**
 * Инициализация базовой клетки
 *
 * @param {object} baseCell
 * @return {array}
 */
function initBaseCell(baseCell) {
  const x = baseCell.getAttribute('x');
  const y = baseCell.getAttribute('y');

  return [{ coordinates: { x: Number(x), y: Number(y) }, item: baseCell }];
}

/**
 * Инициализация значений размеров поля
 *
 * @return {void}
 */
function initSelectSizes(initSize) {
  const select = document.getElementById('selectArenaSize');

  sizeAndSteps.map((value, i) => {
    const opt = document.createElement('option');
    opt.value = i;
    opt.label = value.size;

    if (i === initSize) {
      opt.setAttribute('selected', true);
    }

    select.appendChild(opt);
  });
}

/**
 * Валидация завершенности игры
 *
 * @param {number} all
 * @param {number} curr
 * @param {number} countFilled
 * @param {number} size
 * @return {object}
 */
function validateStateGame(all, curr, countFilled, size) {
  const arena = document.querySelector('#arena');
  console.log(all, curr, countFilled, size);

  if ((countFilled + 1) === size) {
    $(arena).unbind('click');
    alert('You won!');
  }

  if (curr === all && (countFilled + 1) !== size) {
    $(arena).unbind('click');
    alert('You lose!');
  }

  if (curr > all) {
    $(arena).unbind('click');
    alert('You lose!');
  }
}

/**
 * Запрос на создание пользователя
 *
 * @param {string} name
 * @return {object}
 */
async function createUser(name) {
  try {
    return await axios.post(`${BACKEND_URL}/user`, { name });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Получение имени пользователя
 *
 * @return {string}
 */
function setUserName() {
  const username = (prompt('Enter your name:', ''));

  if (!username) {
    setUserName();
  }

  return username;
}

/**
 * Аутентификация пользователя
 *
 * @return {object}
 */
async function authentication() {
  let user = localStorage.getItem('user');

  if (user === null) {
    const username = setUserName();
    const result = await createUser(username);
    localStorage.setItem('user', JSON.stringify(result.data.user));
    user = result.data.user;
  } else {
    user = JSON.parse(user);
  }

  const fieldName = document.querySelector('#username');

  fieldName.innerHTML += user.name;

  return user;
}

/**
 * Рендер количества шагов
 *
 * @param {number} all
 * @param {number} curr
 * @return {void}
 */
function renderSteps(all, curr) {
  const currentStep = document.querySelector('#current-step');
  const allSteps = document.querySelector('#all-steps');

  currentStep.innerHTML = '';
  allSteps.innerHTML = '';

  currentStep.innerHTML += curr;
  allSteps.innerHTML += all;
}

/**
 * Рендер поля
 *
 * @return {void}
 */
function render(size, allSteps) {
  const arena = document.querySelector('#arena');
  const items = arena.children;
  let originCells = [];
  let currentStep = 0;

  reset(arena, size);

  $('#reset').unbind('click').bind('click', () => {
    reset(arena, size);
    renderSteps(allSteps, 0);
    render(size, allSteps);
  });

  $('#save').unbind('click').bind('click', () => save());

  originCells = initBaseCell(items[0]);

  $(arena).unbind('click').bind('click', (event) => {
    if (event.target.classList.contains('square') === false) {
      return;
    }

    currentStep++;

    renderSteps(allSteps, currentStep);

    validateStateGame(allSteps, currentStep, originCells.length, size * size);

    originCells = action(items, originCells, event.target, size);
  });
}

/**
 * Выбор размера поля
 *
 * @param {number} value
 * @return {void}
 */
function onChangeSize(value) {
  const result = sizeAndSteps[value];
  renderSteps(result.steps, 0);
  render(result.size, result.steps);
}

function save() {
  console.info('save');
  //
}

/* Entry */
(function main() {
  authentication().then();

  const { size, steps } = sizeAndSteps[0];

  initSelectSizes(0);
  render(size, steps);
  renderSteps(steps, 0);
}());
